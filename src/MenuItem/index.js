"use strict";

import React from "react";
import classNames from "classnames";

export default class MenuItem extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

        const { condition, url, _classes, text } = this.props;

        if (!condition) return null;

        return (

            <li>
                <a href={ url }>
                    <i className={ classNames(..._classes) }></i>
                    <span>{ text }</span>
                </a>
            </li>

        );

	}

};
