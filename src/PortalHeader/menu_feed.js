"use strict";

export default {
    OFFERS: {
        URL: "offersUrl",
        TEXT: "Ofertas"
    },
    HOTELS: {
        URL: "hotelsUrl",
        TEXT: "Hoteles"
    },
    FLIGHTS: {
        URL: "flightsUrl",
        TEXT: "Vuelos"
    },
    CARS: {
        URL: "carsUrl",
        TEXT: "Autos"
    },
    TOURS: {
        URL: "toursUrl",
        TEXT: "Tours"
    }
}
