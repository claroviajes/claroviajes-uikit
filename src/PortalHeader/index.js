'use strict';

import React from 'react/addons';
import classNames from 'classnames';
import styles from './PortalHeader.local.css';
import * as l10n from '@lsm/claroviajes-l10n';

/* Images */
import logoImg from './images/logo.png';

/* Modules */
import SessionHeader from '../SessionHeader';

export default class PortalHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const brand    = l10n.getCategory(this.props.language, this.props.country, 'brand');

        return (

            <header className={styles.header_main}>

                    <div className="container clearfix">

                        <div className="col-sm-5 col-md-6">

                          <a href={ brand.dotcomUrl } className={ styles.logo }><img alt={ brand.name } src={ logoImg } /></a>

                        </div>

                        <div className="col-xs-12 col-sm-7 col-md-6">

                          <SessionHeader { ...this.props } />

                        </div>

                        <div className="clearfix"></div>

                    </div>


            </header>

		);
	}
};

PortalHeader.defaultProps = {
  offersButtonVisible: true,
  packagesButtonVisible: true,
  hotelsButtonVisible: true,
  flightsButtonVisible: true,
  carsButtonVisible: true,
  toursButtonVisible: true,
  language: 'es',
  country: 'mx'
};
