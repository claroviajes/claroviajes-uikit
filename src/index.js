"use strict";

module.exports = {
	PortalHeader: require('./PortalHeader'),
	SessionHeader: require('./SessionHeader'),
	DetailCanvas: require('./DetailCanvas'),
	DetailMenu: require('./DetailMenu'),
	Background: require('./Background'),
	DetailTitle: require('./DetailTitle'),
	CommonLayout: require('./CommonLayout'),
    ServicesSection: require('./ServicesSection'),
	Footer: require('./Footer')
};
