"use strict";

export default {
	Logo: require('./images/footer-main.logo-claro.png'),
	Callcenter: require('./images/callcenter_blanco.png'),
	App: require('./images/googlePlay.png'),
	Tripadvisor: require('./images/tripadvisor_blanco.png'),
}
