'use strict';

import React from 'react';
import classNames from 'classnames';
import styles from './FooterMx.local.css';
import * as l10n from '@lsm/claroviajes-l10n';

import images from './images';

export default class Footer extends React.Component {

  constructor(props) {
    super(props);
  }

  renderTripAdvisorBrand() {

    let messages = l10n.getCategory(this.props.language, this.props.country, 'messages');

    if (this.props.tripAdvisorBrandEnabled) {
        return (
          <span>
              <p className={ styles.tripadvisor_text }>{messages.commentOf}</p>
              <img src={ images.Tripadvisor } className={ styles.tripadvisor_img } alt="Tripadvisor" />
          </span>
        );
    }

    return null;

  }

  render() {

    let brand = l10n.getCategory(this.props.language, this.props.country, 'brand');
    let services = l10n.getCategory(this.props.language, this.props.country, 'services');
    let messages = l10n.getCategory(this.props.language, this.props.country, 'messages');

    console.log(services);
    console.log(messages.noticeOfPrivacy);


		return (

            <div>
                <div className="clearfix"></div>
                <footer className={ styles.footer_top }>
                   <div className="container">
                      <div className="row">

                          <div className="col-xs-12 col-sm-3 col-md-3">
                            <img src={brand.footerLogo} className={ styles.claro_logo } alt={brand.name} />
                          </div>
                          <div className="clearfix"></div>

                          <div className="col-xs-12 col-sm-3 col-md-3">
                             <p className={ styles.copy }>@2016 {brand.name}</p>
                             <p className={ styles.links }><a href={ brand.eulaUrl }>{messages.termsAndConditions}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p>
                             <p className={ styles.links }><a href={ brand.privacyUrl }>{messages.noticeOfPrivacy}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p>
                             <p className={ styles.links }><a href={ brand.telcoUrl }>{messages.goTo + brand.telcoName } <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p>
                          </div>

                          <div className="col-xs-12 col-sm-3 col-md-3">
                            <p className={ styles.links }><a href={ brand.contactUrl }>{messages.contactUs}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a> </p>
                              { (brand.assistEnabled) ? <p className={ styles.links }><a href={ brand.assistUrl }>{messages.travelersAssistance}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.faqsEnabled) ? <p className={ styles.links }><a href={ brand.faqsUrl }>{messages.frequentlyAskedQuestions}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.paymentEnabled) ? <p className={ styles.links }><a href={ brand.paymentUrl }>{messages.paymentsMeans}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.winnersEnabled) ? <p className={ styles.links }><a href={ brand.winnersUrl }>{messages.winners}<i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                          </div>

                          <div className="col-xs-12 col-sm-3 col-md-3">
                              { (brand.facebookEnabled) ? <p className={ styles.links }><a href={ brand.facebookHandle } title="Facebook" target="_blank"><i className="fa fa-facebook"></i> Facebook <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.twitterEnabled) ? <p className={ styles.links }><a href={ brand.twitterHandle } title="Twitter" target="_blank"><i className="fa fa-twitter"></i> Twitter <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.youtubeEnabled) ? <p className={ styles.links }><a href={ brand.youtubeHandle } title="Youtube" target="_blank"><i className="fa fa-youtube-play"></i> Youtube <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              <span className={ styles.line }></span>
                              { (brand.googlePlayEnabled) ? <p className={ styles.links }><a href={ brand.googlePlayUrl } title="Google Play" target="_blank"><i className="fa fa-android"></i> Google Play <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                              { (brand.appStoreEnabled) ? <p className={ styles.links }><a href={ brand.appStoreUrl } title="Apple Store" target="_blank"><i className="fa fa-apple"></i> Apple Store <i className={ classNames( "fa fa-angle-right", styles.fa_mobile )}></i></a></p> : null }
                          </div>

                          <div className="col-xs-12 col-sm-3">

                            <div className={ styles.caption_callcenter }>
                                <span className={ styles.caption_callcenter__title }>{messages.dubtsContactUs}</span>
                                <span className={ styles.caption_callcenter__phone }>{brand.callcenterNumber}</span>
                                <span className={ styles.caption_callcenter__text }>{brand.callcenterTimeRange}</span>
                            </div>

                          </div>

                      </div>
                  </div>
                </footer>
                    <footer className={ styles.footer_bottom }>
                        <div className="container">
                           <div className="row">
                             <div className="col-xs-2 col-sm-2 col-md-1">
                                 <img src={ images.Logo } className={ styles.img_logo } />
                             </div>
                               <div className="col-xs-10 col-sm-6 col-md-7">
                                   <p className={ styles.compania }>{ brand.responsibleCompany }</p>
                                   <p className={ styles.compania }>{messages.responsableTurismOperatorIs + brand.touristOperator }</p>
                               </div>
                               <div className="col-xs-12 col-sm-4 col-md-4">
                                   { this.renderTripAdvisorBrand() }
                               </div>
                           </div>
                       </div>
                    </footer>

            </div>

		);

	}

};


Footer.defaultProps = {
  offersButtonVisible: true,
  packagesButtonVisible: true,
  hotelsButtonVisible: true,
  flightsButtonVisible: true,
  carsButtonVisible: true,
  toursButtonVisible: true,
  language: 'es',
  country: 'mx'
};
