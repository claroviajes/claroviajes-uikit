"use strict";

import React from "react";
import classNames from "classnames";

import NavMobile from '../NavMobile';
import PortalHeader from '../PortalHeader';
// import SessionHeader from '../SessionHeader';
import Nav from '../Nav';
import Background from '../Background';
import Footer from '../Footer';

import styles from "./.local.css";

export default class CommonLayout extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (

            <div className="app">

                <NavMobile { ...this.props } />

                <PortalHeader { ...this.props } />

                {/*<SessionHeader { ...this.props } />*/}

                <Nav { ...this.props } />

                <Background>

                { this.props.children }

                </Background>

                <Footer { ...this.props } />

            </div>

        );

    }

};
