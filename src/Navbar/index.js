"use strict";

import React from "react";
import * as l10n from '@lsm/claroviajes-l10n';

/* Styles */
import styles from "./.local.css";

export default class Navbar extends React.Component {

    constructor(props) {
        super(props);
    }

	render() {

        const brand = l10n.getCategory(this.props.language, this.props.country, 'brand'),
            services = l10n.getCategory(this.props.language, this.props.country, 'services'),
            messages = l10n.getCategory(this.props.language, this.props.country, 'messages');


		return (

			<div>
				<nav className={ styles.navMain }>
					<div className="container">
						<a href={ `${brand.dotcomUrl}` } className={ styles.logo }>
							<img alt={ `${ brand.name }` } src={ brand.logoBrand_118x53} />
						</a>
						<ul className={"nav nav-pills " + styles.menu}>
							<li>
								<a href={ services.offersUrl }>
									<i className={ styles.offers }></i>
									<span>{messages.offers}</span>
								</a>
							</li>
							<li>
								<a href={ services.hotelsUrl }>
									<i className={ styles.hotels }></i>
									<span>{messages.hotels}</span>
								</a>
							</li>
							<li>
								<a href={ services.flightsUrl }>
									<i className={ styles.flights }></i>
									<span>{messages.flights}</span>
								</a>
							</li>
							<li>
								<a href={ services.carsUrl }>
									<i className={ styles.car }></i>
									<span>{messages.cars}</span>
								</a>
							</li>
							<li>
								<a href={ services.toursUrl }>
									<i className={ styles.tours }></i>
									<span>{messages.tours}</span>
								</a>
							</li>
						</ul>
					</div>
				</nav>
				<div className={ styles.line }></div>
			</div>
		);

	}
};
