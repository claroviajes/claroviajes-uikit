"use strict";

import React from "react";
import classNames from "classnames";

import styles from "./.local.css";

export default class DetailTitle extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

		return (

			<h4 className={ styles.wrapper }>
                <span>{ this.props.children }</span>
            </h4>

		);

	}

}
