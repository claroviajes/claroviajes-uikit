"use strict";

import React,  { Component } from "react";
import CSSModules from "react-css-modules";
import { Link } from "react-router";
import classNames from "classnames";

/* Styles */
import styles from "./ProductCard.local.css";

/* Modules */
import RateList from "../RateList/RateList";

export default class ProductCard extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		return (

			<article className="col-xs-12 col-sm-3">
			<Link to={ `http://www.google.com` }>
				<header>
					<div className={ styles.oferta_special }><p className={ styles.oferta_special__text }>-60%</p></div>
					<img src="http://aff.bstatic.com/images/hotel/max300/142/14268543.jpg" className="img-responsive" />
				</header>
				<footer>
						<div className={ classNames( "col-xs-12 col-sm-12", styles.title_cont )}>
							<h5 className={ styles.title_hotel }>The Royal Islander deluxe explndis</h5>
							<ul className={ styles.list_rate }>
							  <li><span className={ styles.rate }></span></li>
							  <li><span className={ styles.rate }></span></li>
							  <li><span className={ styles.rate }></span></li>
							  <li><span className={ styles.rate }></span></li>
							  <li><span className={ styles.rate_empty }></span></li>
							</ul>
						</div>
						  <div className="col-xs-12 col-sm-6">
							<div styleName="trip-comments">
								<span className="tripadv">
									<img src="http://www.tripadvisor.com.mx/img/cdsi/img2-daodao/branding/daodao21x12-14894-5.png" />
									<span className={ styles.reviews }>{ 2 }<small>/5</small></span>
								</span>
							</div>
						  </div>
						  <div className="col-xs-12 col-sm-6">
							<p className={ styles.old_price }>MXN$ 1,349</p>
							<p className={ styles.main_price }>MXN$ <span className={ styles.main_price__dest }>1,349</span></p>
						  </div>
				</footer>
			</Link>
			</article>

		);
	}
};

ProductCard.propTypes = {
     offer: React.PropTypes.object
 };

export default CSSModules(ProductCard, styles);
