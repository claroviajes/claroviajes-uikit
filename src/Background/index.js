"use strict";

import React from "react";
import classNames from "classnames";

import styles from "./.local.css";

export default class Background extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

		return (
			<div className={ styles.wrapper }>
        { this.props.children }
			</div>
		);

	}

};
