"use strict";

import React from "react";
import classNames from "classnames";

/* Styles */
import styles from "./HotelCard.local.css";

/* Modules */
import RateList from "../RateList/RateList";

export default class HotelCard extends React.Component {

	constructor(props) {
		super(props);
		this.state = { detailURL: "/results/8" };
	}

	render() {

		let hotel         = this.props.hotel,
			specialOffter = (hotel.discount) ?
							<div className={ styles.specialOffer }>
								<p>-<span>{hotel.discount}</span>%</p>
							</div> : null,
			stars = [];

		for (var i = 1; i <= hotel.stars; i+=1) {
			stars.push(<i key={i} className="fa fa-star"></i>);
		};

		return (
			<div className="col-md-4">
				<div className={ styles.card }>

					<a href="">
						<div className={ styles.header }>
							{ specialOffter }
							<img src={hotel.image} alt={hotel.name} />
						</div>
						<div className={ styles.itemBody }>
							<h4>{ hotel.name }</h4>

							{stars}

							<p>{ hotel.description }</p>
						</div>
					</a>
					<div className={ styles.itemFooter }>
						<a href="#">
							<div className="col-xs-6">
								<p>MXN$
									<span className={ styles.price }>{ hotel.price }</span>
								</p>
							</div>
						</a>
						<div className="col-xs-6">
							<a className={ styles.reserve } href="#">Reservar</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
};

HotelCard.defaultProps = {
	hotel: {
    	name: "Untitled Hotel",
    	image: "https://s3-sa-east-1.amazonaws.com/repos3prd/a0a817acc2cd65406ff576c1a622320b1439308134284.jpg?mode=crop&scale=both&format=jpg&progressive=true",
    	stars: 1,
    	price: 111,
    	description: "no description for this hotel"
	}
};
