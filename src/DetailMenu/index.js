"use strict";

import React from "react";
import classNames from "classnames";
import ALL_OPTIONS from "./options";

import styles from "./.local.css";

export default class DetailMenu extends React.Component {

    constructor(props) {
        super(props);
    }

    renderOptions() {

        let { options } = this.props;

        return options.map((option, i) => {

            if (!ALL_OPTIONS[option.key]) return null;

            return (
                <li key={ i }>
                    <a href={ `#${ option.id }` } className={ classNames({ [styles.highlight]: !!option.highlight }) }>
                        { ALL_OPTIONS[option.key] }
                    </a>
                </li>
            );

        });

    }

    render() {

		return (

			<ul className={ styles.list }>
                { this.renderOptions() }
			</ul>

		);

	}

};
