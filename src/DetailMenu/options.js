"use strict";

export default {
    flight: "el vuelo",
    car: "el auto",
    tour: "el tour",
    hotel: "el hotel",
    cruise: "el crucero",
    rooms: "cuartos",
    services: "servicios",
    location: "ubicación",
    regulations: "regulación de tarifa",
    additional_info: "información adicional",
    benefits: "beneficios",
    enquiry: "consultar"
};
