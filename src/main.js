import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';

import Navbar from './Navbar';
import SessionHeader from './SessionHeader';
import Separator from './Separator';
import PortalHeader from './PortalHeader';
import OfferDetailBanner from './OfferDetailBanner';
import HotelCard from './HotelCard';

import offer_status_types from '../config/types/offer_status';

import bannerImg from './OfferDetailBanner/images/top.jpg';

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    clickedBook(booked) {
        alert("Booked: ", booked);
    }

    render() {

        let inTenDays   = moment().add(10, 'd').format(),
            inOneMinute = moment().add(1, 'm').format(),
            expired     = moment().subtract(1, 'd').format();

        return (

            <div className="app">

            <PortalHeader toursButtonUrl={ "http://www.google.com" } />

                <Separator />
                <SessionHeader />
                <OfferDetailBanner
                offerType={ "Hotel" }
                offerStatus={ offer_status_types.ACTIVE }
                isClubOffer={ true }
                backgroundImage={ bannerImg }
                title={ "Los Cabos" }
                caption={ "¡Continua el Buen Fin! Viaja a Los Cabos a un precio imperdible." }
                discount={ 70 }
                finalPrice={ 4.974 }
                scratchedPrice={ 12.974 }
                saving={ 8.546 }
                expirationDate={ inOneMinute }
                onBookClick={ this.clickedBook.bind(this) }

            />

            </div>

        );
    }
}

ReactDOM.render(<App />, document.getElementById('content'));
