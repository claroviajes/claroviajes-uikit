"use strict";

import React from "react/addons";
import classNames from "classnames";
import FEED from "../Nav/menu_feed";
import * as l10n from '@lsm/claroviajes-l10n';

/* Styles */
import PortalHeaderStyles from "../PortalHeader/PortalHeaderAll.local.css";
import styles from "./.local.css";

/* Modules */
import MenuItem from "../MenuItem";

export default class NavMobile extends React.Component {

	constructor(props) {
        super(props);
        this.state = { showMenu: false };
        this.hideMenu = this.hideMenu.bind(this);
	}

	showMenu() {
        this.setState({ showMenu: true });
        document.addEventListener("click", this.hideMenu);
    }

    hideMenu() {
        document.removeEventListener("click", this.hideMenu);
        this.setState({ showMenu: false });
    }

    getBodyHeight() {
        let body = document.body, html = document.documentElement;
        return Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    }

	render() {

        const services = l10n.getCategory(this.props.language, this.props.country, 'services');

		let showMenuClass = classNames({
	      [styles.show]: this.state.showMenu
	    }),

        heightStyle = { height: `${this.getBodyHeight()}px` };

		return (

            <div className={ styles.menu }>

                <button onClick={ this.showMenu.bind(this) }>
                    <span className={ styles.bar }></span>
                    <span className={ styles.bar }></span>
                    <span className={ styles.bar }></span>
                </button>

    			<div className={ classNames("responsive-menu", styles.responsive, showMenuClass) }>

    				<ul style={ heightStyle }>

                        <MenuItem
                            condition={ services.offersEnabled }
                            url={ this.props[FEED.OFFERS.URL] || services[FEED.OFFERS.URL] }
                            _classes={ [ PortalHeaderStyles.icon_nav, PortalHeaderStyles.icon_ofertas ] }
                            text={ FEED.OFFERS.TEXT }
                        />

                        <MenuItem
                            condition={ services.hotelsEnabled }
                            url={ this.props[FEED.HOTELS.URL] || services[FEED.HOTELS.URL] }
                            _classes={ [ PortalHeaderStyles.icon_nav, PortalHeaderStyles.icon_hoteles ] }
                            text={ FEED.HOTELS.TEXT }
                        />

                        <MenuItem
                            condition={ services.flightsEnabled }
                            url={ this.props[FEED.FLIGHTS.URL] || services[FEED.FLIGHTS.URL] }
                            _classes={ [ PortalHeaderStyles.icon_nav, PortalHeaderStyles.icon_vuelos ] }
                            text={ FEED.FLIGHTS.TEXT }
                        />

                        <MenuItem
                            condition={ services.carsEnabled }
                            url={ this.props[FEED.CARS.URL] || services[FEED.CARS.URL] }
                            _classes= { [ PortalHeaderStyles.icon_nav, PortalHeaderStyles.icon_autos ]  }
                            text={ FEED.CARS.TEXT }
                        />

                        <MenuItem
                            condition={ services.toursEnabled }
                            url={ this.props[FEED.TOURS.URL] || services[FEED.TOURS.URL] }
                            _classes={ [ PortalHeaderStyles.icon_nav, PortalHeaderStyles.icon_tours ] }
                            text={ FEED.TOURS.TEXT }
                        />

    	            </ul>

    			</div>

            </div>


		);
	}

};
