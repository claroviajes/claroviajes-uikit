"use strict";

export default {
    OFFERS: {
        URL: "offersUrl",
        TEXT: "offers"
    },
    HOTELS: {
        URL: "hotelsUrl",
        TEXT: "hotels"
    },
    FLIGHTS: {
        URL: "flightsUrl",
        TEXT: "flights"
    },
    CARS: {
        URL: "carsUrl",
        TEXT: "cars"
    },
    TOURS: {
        URL: "toursUrl",
        TEXT: "tours"
    }
}
