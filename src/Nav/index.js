'use strict';

import React from 'react/addons';
import classNames from 'classnames';
import styles from './Nav.local.css';
import FEED from "./menu_feed";
import * as l10n from '@lsm/claroviajes-l10n';

/* Modules */
import MenuItem from "../MenuItem";

/* Images */
import logoImg from './images/logo.png';

export default class PortalHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const brand    = l10n.getCategory(this.props.language, this.props.country, 'brand'),
            services = l10n.getCategory(this.props.language, this.props.country, 'services'),
            messages = l10n.getCategory(this.props.language, this.props.country, 'messages');


        return (

            <header className={styles.header_nav}>

                <nav className={styles.nav_main}>

                    <div className="container clearfix">

                        <ul className={ classNames("nav", styles.nav_pills, styles.menu_xl ) }>

                            <MenuItem
                                condition={ services.offersEnabled }
                                url={ this.props[FEED.OFFERS.URL] || services[FEED.OFFERS.URL] }
                                _classes={ [ styles.icon_nav, styles.icon_ofertas ] }
                                text={ messages[FEED.OFFERS.TEXT] }
                            />

                            <MenuItem
                                condition={ services.hotelsEnabled }
                                url={ this.props[FEED.HOTELS.URL] || services[FEED.HOTELS.URL] }
                                _classes={ [ styles.icon_nav, styles.icon_hoteles ] }
                                text={ messages[FEED.HOTELS.TEXT] }
                            />

                            <MenuItem
                                condition={ services.flightsEnabled }
                                url={ this.props[FEED.FLIGHTS.URL] || services[FEED.FLIGHTS.URL] }
                                _classes={ [ styles.icon_nav, styles.icon_vuelos ] }
                                text={ messages[FEED.FLIGHTS.TEXT] }
                            />

                            <MenuItem
                                condition={ services.carsEnabled }
                                url={ this.props[FEED.CARS.URL] || services[FEED.CARS.URL] }
                                _classes= { [ styles.icon_nav, styles.icon_autos ]  }
                                text={ messages[FEED.CARS.TEXT] }
                            />

                            <MenuItem
                                condition={ services.toursEnabled }
                                url={ this.props[FEED.TOURS.URL] || services[FEED.TOURS.URL] }
                                _classes={ [ styles.icon_nav, styles.icon_tours ] }
                                text={ messages[FEED.TOURS.TEXT] }
                            />

                        </ul>

                    </div>

                </nav>

            </header>

		);
	}
};

PortalHeader.defaultProps = {
  offersButtonVisible: true,
  packagesButtonVisible: true,
  hotelsButtonVisible: true,
  flightsButtonVisible: true,
  carsButtonVisible: true,
  toursButtonVisible: true,
  language: 'es',
  country: 'mx'
};
