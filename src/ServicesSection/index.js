"use strict";

import React from "react";
import classNames from "classnames";

import styles from "./.local.css";

export default class ServiceItem extends React.Component {

    constructor(props) {
        super(props);
    }

    renderServices() {

        return this.props.services.map((service, i) => {
            return (

                <li key={ i }>
                    <i className={ classNames(styles.sprite, styles[service]) }></i>
                    <span className={ styles.title }>{ service.charAt(0).toUpperCase() + service.slice(1) }</span>
                </li>

            );
        });

    }

    render() {

		return (

            <ul className={ styles.list }>
                { this.renderServices() }
            </ul>

		);

	}

}
