"use strict";

import React from "react";

import styles from "./.local.css";

export default class Separator extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {

		return (

      <div className={ styles.separator }></div>

		);
	}

};
