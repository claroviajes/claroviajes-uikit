"use strict";

import React from "react";
import classNames from "classnames";

/* Styles */
import styles from "./RateList.local.css";

/* Modules */
import IconRating from "react-icon-rating";

export default class RateList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {

		let rates = this.props.rates.map((rate) => {
					 <li key={rate.id}>
					 	<span className={ styles.rate }></span><span className={ styles.rateEmpty }></span>
					 </li>
					});

		return (
			<ul className={ styles.rateList }>
				{rates}
			</ul>
		);
	}
};
