"use strict";

import React from "react";
import classNames from "classnames";

import styles from "./.local.css";

export default class DetailCanvas extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

		return (
      <div className={ classNames(styles.wrapper, 'container') }>
			  { this.props.children }
      </div>
		);

	}

};
