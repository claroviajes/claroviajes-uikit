'use strict';

import React from 'react';
import classNames from 'classnames';
import styles from './SessionHeaderMx.local.css';
import * as l10n from '@lsm/claroviajes-l10n';
import { DropdownButton, MenuItem } from 'react-bootstrap';

import images from './images';

export default class SessionHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    logout(e) {
        e.preventDefault();
        this.props.logout();
    }

    renderSession() {

        const { language, country, redirectUrl, user } = this.props,
            services = l10n.getCategory(language, country, 'services'),
            messages = l10n.getCategory(language, country, 'messages');


        let loginUrl = services.loginUrl || `${services.accountUrl}/login?url=${redirectUrl || ""}`,
            subscribeUrl = services.subscribeUrl || `${services.accountUrl}/subscribe?url=${redirectUrl || ""}`,
            profileUrl = `${services.accountUrl}/profile`,
            casesUrl = `${services.accountUrl}/profile#bookings`;

        if (!user || !user.firstName) {
            return (
                <div>
                    <a href={ loginUrl } className={styles.caption_sesion}>{messages.logIn}</a>
                    <a href={ subscribeUrl } className={styles.caption_subscribe}>{messages.signIn}</a>
                </div>
            );
        }

        return (
            <DropdownButton bsStyle="warning" noCaret title={`${ user.firstName } ${ user.lastName }`} id="profile-dropdown">
                <MenuItem href={ profileUrl }>
                    <i className="fa fa-user" /> {messages.editProfile}
                </MenuItem>
                <MenuItem href={ casesUrl }>
                    <i className="fa fa-calendar-check-o" /> {commons.myBookings}
                </MenuItem>
                <MenuItem href="javascript:;" onClick={ this.logout.bind(this) }>
                    <i className="fa fa-sign-out" /> {commons.logOut}
                </MenuItem>
            </DropdownButton>
        );

    }

    render() {

        const brand = l10n.getCategory(this.props.language, this.props.country, 'brand');

        return (
            <div className={styles.sesion_main}>

                    <div className={styles.caption_user}>

                        <i className={styles.icon_callcenter}></i>
                        <span>{brand.callcenterNumber}</span>

                    </div>

                    <div className={styles.caption_nav}>

                        { this.renderSession() }

                    </div>
                    <div className="clearfix"></div>


            </div>
  		);
  	}

};

SessionHeader.defaultProps = {
    language: 'es',
    country: 'mx',
    user: null,
    redirectUrl: null,
    accountUrl: "/"
};
