/*
 * Webpack development server configuration
 *
 * This file is set up for serving the webpack-dev-server, which will watch for changes and recompile as required if
 * the subfolder /webpack-dev-server/ is visited. Visiting the root will not automatically reload.
 */
'use strict';
var webpack = require('webpack');
var path = require('path');

module.exports = {

  output: {
    path: './lib',
    filename: 'index.js',
    libraryTarget: 'umd',
    library: 'claroviajes-uikit'
  },

  externals: {
    react: 'react',
    'react/addons': 'react',
    moment: 'moment'
  },

  cache: true,
  debug: false,
  entry: path.resolve(__dirname, 'src/index.js'),

  stats: {
    colors: true,
    reasons: true
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'components': __dirname + '/src/'
    }
  },
  module: {
    // preLoaders: [{
    //   test: /\.(js|jsx)$/,
    //   exclude: /node_modules/,
    //   loader: 'eslint-loader'
    // }],
    loaders: [{
      test: /\.global.css$/,
      loader: 'style-loader!css-loader'
    }, {
      test: /local.css$/,
      loader: 'style-loader!css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]__[hash:64]!postcss-loader'
    }, {
      test: /\.(js|es6|jsx)$/,
      exclude: /node_modules/,
      loader: 'react-hot!babel-loader'
    }, {
      test: /\.(png|woff|woff2|svg|otf|ttf|eot)$/,
      loader: 'url-loader?limit=15000'
    }, {
      test: /\.(jpg)$/,
      loader: 'file-loader?name=images/[name].[ext]'
    }]
  },

  postcss: [
    require('autoprefixer')
  ],

  plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': { 'NODE_ENV': '"production"' }
    })
  ]

};
