'use strict';

export default {
	ANONYMOUS: 'anonymous',
	ACTIVE: 'active',
	BOOKED: 'booked'
}