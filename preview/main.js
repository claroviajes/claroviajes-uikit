"use strict"

import React from 'react';
import moment from 'moment';

require("./.global.css");

import Footer from '../src/Footer';
import CommonLayout from '../src/CommonLayout';
// import OfferDetailBanner from '../src/OfferDetailBanner';
import DetailCanvas from '../src/DetailCanvas';
// import DetailMenu from '../src/DetailMenu';
// import DetailTitle from '../src/DetailTitle';
// import OfferInfoSection from '../src/OfferInfoSection';
// import OfferLocationSection from '../src/OfferLocationSection';
// import OfferQuoteSection from '../src/OfferQuoteSection';
// import OfferRoomSection from '../src/OfferRoomSection';
// import OfferInquirySection from '../src/OfferInquirySection';
// import OfferCommentsSection from '../src/OfferCommentsSection';
// import OfferConditionsSection from '../src/OfferConditionsSection';

// import offer_status_types from '../config/types/offer_status';

import bannerImg from './images/top.jpg';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  clickedBook(booked) {
    alert("Booked: ", booked);
  }

  render() {

    let inTenDays   = moment().add(10, 'd').format(),
    inOneMinute     = moment().add(1, 'm').format(),
    expired         = moment().subtract(1, 'd').format();

    return (

        <CommonLayout country="pa">

          {/* Change: offerStatus, isClubOffer and expirationDate to see all offer stages
          <OfferDetailBanner
           offerType={ "Hotel" }
           offerStatus={ offer_status_types.ACTIVE }
           isClubOffer={ true }
           backgroundImage={ bannerImg }
           title={ "Los Cabos" }
           caption={ "¡Continua el Buen Fin! Viaja a Los Cabos a un precio imperdible." }
           discount={ 70 }
           finalPrice={ 4.974 }
           scratchedPrice={ 12.974 }
           saving={ 8.546 }
           expirationDate={ expired }
           onBookClick={ this.clickedBook.bind(this) }
          />*/}

          <DetailCanvas>

            {/*div className="row">
              <DetailMenu />
            </div>

            <DetailTitle>Example of grid columns inside Detail canvas</DetailTitle>
            */}

            <div className="row">
              <div className="col-md-4" style={ { backgroundColor: 'red'} }>
                aaa
              </div>
              <div className="col-md-4" style={ { backgroundColor: 'green'} }>
                bbb
              </div>
              <div className="col-md-4" style={ { backgroundColor: 'blue'} }>
                bbb
              </div>
            </div>


          {/*  <OfferInfoSection />
            <OfferLocationSection />
            <OfferQuoteSection />
            <OfferRoomSection />
            <OfferInquirySection />
            <OfferCommentsSection />
            <OfferConditionsSection />
                  */}

          </DetailCanvas>


        </CommonLayout>

      )
  }
}

let content = document.getElementById('content');
React.render(<App />, content);
