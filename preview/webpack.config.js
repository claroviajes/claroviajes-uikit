/*
 * Webpack development server configuration
 *
 * This file is set up for serving the webpack-dev-server, which will watch for changes and recompile as required if
 * the subfolder /webpack-dev-server/ is visited. Visiting the root will not automatically reload.
 */
'use strict';
var webpack = require('webpack');
var path = require('path');

module.exports = {

  output: {
    filename: 'main.js',
    publicPath: "/assets/",
    path: "./dist"
  },

  devServer: {
    hot: true,
    port: 8000,
    publicPath: '/assets/',
    contentBase: './preview/',
    historyApiFallback: true
  },

  cache: true,
  debug: true,
  devtool: 'source-map',
  entry: [
      __dirname + '/main.js',
      'webpack/hot/only-dev-server'
  ],

  stats: {
    colors: true,
    reasons: true
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      'components': __dirname + '/src/'
    }
  },
  module: {
    preLoaders: [{
      test: /\.(js|jsx)$/,
      exclude: /(node_modules|maqueta)/,
      loader: 'eslint-loader'
    }],
    loaders: [{
      test: /\.sass/,
      loader: 'style-loader!css-loader?modules&sourceMap!sass-loader?outputStyle=expanded&indentedSyntax'
    }, {
      test: /\.global.css$/,
      loader: 'style-loader!css-loader'
    }, {
      test: /\local.css/,
      loader: 'style-loader!css-loader?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]__[hash:64]!postcss-loader'
    }, {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'react-hot!babel-loader'
    }, {
      test: /\.(png|gif|woff|woff2|svg|otf|ttf|eot)$/,
      loader: 'url-loader?limit=8192'
    }, {
      test: /\.(jpg)$/,
      loader: 'file-loader?name=images/[name].[ext]'
    }]
  },

  postcss: [
    require('autoprefixer')
  ],

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]

};
