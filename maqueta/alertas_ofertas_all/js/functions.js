/* menu responsive */
$('#menu').slicknav();


/* placeholder ie8 */
$('input').placeholder();

/* campos input numericos */
$('.telcel_number').numeric();



$(".caption-register, .caption-signin, .caption-sesion, .caption-subscribe").click(function () {
    $(".well-suscripcion").show("slow");
    return false;
});

$(".icon-close-container" ).click(function() {
    $(".well-suscripcion").hide("slow");
    return false;
});



/* ofertas */
$(".card-oferta").hover(
  function() {
    $(".card-oferta").removeClass("active");
    $(this).addClass("active");
  }, function() {
    $(".card-oferta").removeClass("active");
  }
);

/* alertas */
$(".card-alerta").hover(
  function() {
    $(".card-alerta").removeClass("active");
    $(this).addClass("active");
  }, function() {
    $(".card-alerta").removeClass("active");
  }
);


/* owl carousel */
$(".owl-carousel").owlCarousel({
    items : 3,
    // itemsCustom : false,
    // itemsDesktop : [1199,4],
    // itemsDesktopSmall : [980,3],
    // itemsTablet: [768,2],
    // itemsTabletSmall: false,
    // itemsMobile : [479,1],
    // responsive: true,
    // responsiveRefreshRate : 200,
    // responsiveBaseWidth: window,
    mouseDrag: false,
    navigation: true,
    navigationText: [
    "<span class='owl-icon-carousel icon-carousel-left'></span>",
    "<span class='owl-icon-carousel icon-carousel-right'></span>"
    ],
    rewindNav: true
});






/* ========================================================================
 * SUSCRIPTION
 * ======================================================================== */

/* size - opacity promo */
var $container  = $('.box-productos'),
  $articles = $container.children('a'),
  timeout;

$articles.on( 'mouseenter', function( event ) {
  var $article  = $(this);
  clearTimeout( timeout );
  timeout = setTimeout( function() {
    if( $article.hasClass('active') ) return false;
    $articles.not( $article.removeClass('blur').addClass('active') )
    .removeClass('active')
    .addClass('blur');
  }, 65 );
});

$container.on( 'mouseleave', function( event ) {
  clearTimeout( timeout );
  $articles.removeClass('active blur');
});


/* get URL Param */
function getUrlParameter(sParam) {
   var sPageURL = window.location.search.substring(1);
   var sURLVariables = sPageURL.split('&');
   for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
};
var fromFB = getUrlParameter('fromFB');
if ( fromFB == "true" ){
    $('#dashboard-subscription').addClass('dashboard-subscription-fb');
    $('#bodyFB').addClass('body-fb');
    $('a.btn-ver-ofertas').attr('target', '_blank');
};

if ( window.location !== window.parent.location ) {
    //console.log('in a frame');
    $('#dashboard-subscription').addClass('dashboard-subscription-fb');
    $('#bodyFB').addClass('body-fb');
    $('a.btn-ver-ofertas').attr('target', '_blank');
} else {
    //console.log('not in a frame ')
}

/* ------------ js suscripciopn end -------------- */



/*
SEARCH HOME
*/

$('#selector-search').on('change',function(){
    var valor = $(this).val();
    //console.log(valor);
    switch(valor){
        case "hotel":
            $('.search-hotel').show();
            $('.search-vuelo').hide();
            break;
        case "vuelo":
            $('.search-hotel').hide();
            $('.search-vuelo').show();
            break;
        default:
            $('.search-hotel').show();
            $('.search-vuelo').hide();
    }
});


$(document).ready(function () {

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);


    var fromDate = new Date();
    var toDate = new Date();

    var today = new Date();
    var tomorrow = new Date(today);
    fromDate.setDate(today.getDate() + 2);

    $("#campofecha-first").datepicker({
        showOn: 'both',
        buttonImage: './img/home/calendario.png',
        buttonImageOnly: true,
        changeYear: false,
        numberOfMonths: 2,
        beforeShowDay: function (date) {

            if (fromDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];
            if (toDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];

            if (toDate != null && date.getTime() >= fromDate.getTime() &&
                             date.getTime() <= toDate.getTime()) {
                return [true, "selected-date"];
            }
            else
                return [true, ""];



        },
        dateFormat: 'dd/mm/yy',
        minDate: fromDate,
        onSelect: function (textoFecha, objDatepicker) {
            fromDate = $(this).val().toString();
            fromDate = $.datepicker.parseDate('dd/mm/yy', textoFecha);
            $("#campofecha-first").change();

            //refresh date to
            var toDate2 = new Date(fromDate);
            toDate2.setDate(fromDate.getDate() + 2);
            $('#campofecha-last').datepicker('option', 'minDate', toDate2);


        }

    });


    $("#campofecha-last").datepicker({
        showOn: 'both',
        buttonImage: './img/home/calendario.png',
        buttonImageOnly: true,
        changeYear: false,
        numberOfMonths: 2,
        beforeShowDay: function (date) {

            if (fromDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];
            if (toDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];


            if (fromDate != null && date.getTime() >= fromDate.getTime() &&
                              date.getTime() <= toDate.getTime()) {
                return [true, "selected-date"];
            }
            else
                return [true, ""];


        },
        dateFormat: 'dd/mm/yy',
        minDate: toDate,
        onSelect: function (textoFecha, objDatepicker) {
            toDate = $(this).val().toString();
            toDate = $.datepicker.parseDate('dd/mm/yy', textoFecha);
            $("#campofecha-last").change();
        }
    });



    $("#campofecha-vuelo-first").datepicker({
        showOn: 'both',
        buttonImage: './img/home/calendario.png',
        buttonImageOnly: true,
        changeYear: false,
        numberOfMonths: 2,
        beforeShowDay: function (date) {

            if (fromDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];
            if (toDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];

            if (toDate != null && date.getTime() >= fromDate.getTime() &&
                             date.getTime() <= toDate.getTime()) {
                return [true, "selected-date"];
            }
            else
                return [true, ""];

        },
        dateFormat: 'dd/mm/yy',
        minDate: fromDate,
        onSelect: function (textoFecha, objDatepicker) {
            fromDate = $(this).val().toString();
            fromDate = $.datepicker.parseDate('dd/mm/yy', textoFecha);
            $("#campofecha-vuelo-first").change();

            //refresh date to
            var toDate2 = new Date(fromDate);
            toDate2.setDate(fromDate.getDate() + 2);
            $('#campofecha-vuelo-last').datepicker('option', 'minDate', toDate2);
        }

    });

    $("#campofecha-vuelo-last").datepicker({
        showOn: 'both',
        buttonImage: './img/home/calendario.png',
        buttonImageOnly: true,
        changeYear: false,
        numberOfMonths: 2,
        beforeShowDay: function (date) {

            if (fromDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];
            if (toDate.getTime() == date.getTime())
                return [true, "selected-date ui-datepicker-current-day"];


            if (fromDate != null && date.getTime() >= fromDate.getTime() &&
                              date.getTime() <= toDate.getTime()) {
                return [true, "selected-date"];
            }
            else
                return [true, ""];


        },
        dateFormat: 'dd/mm/yy',
        minDate: toDate,
        onSelect: function (textoFecha, objDatepicker) {
            toDate = $(this).val().toString();
            toDate = $.datepicker.parseDate('dd/mm/yy', textoFecha);
            $("#campofecha-vuelo-last").change();
        }
    });

    $("#strDestinos").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "http://hoteles.claroviajes.com/search/getplaces",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.PlaceName,
                            value: item.PlaceId
                        };
                    }));
                }
            });
        },
        minLength: 2,
        focus: function (event, ui) {
            $(event.target).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $(event.target).val(ui.item.label);
            $(event.target).attr("placeId", ui.item.value);
            clearValidationsHotel();
            return false;
        }
    });


    //vuelos
    $("#strVuelos-origen").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "http://vuelos.claroviajes.com/search/getairports",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.PlaceName,
                            value: item.PlaceId,
                            airport: item.Code
                        };
                    }));
                }
            });
        },
        minLength: 2,
        focus: function (event, ui) {
            $(event.target).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $(event.target).val(ui.item.label);
            $(event.target).attr("placeId", ui.item.value);
            $(event.target).attr("airport", ui.item.airport);
            clearValidationsFlight();
            return false;
        }
    });

    $("#strVuelos-destino").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "http://vuelos.claroviajes.com/search/getairports",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            label: item.PlaceName,
                            value: item.PlaceId,
                            airport: item.Code
                        };
                    }));
                }
            });
        },
        minLength: 2,
        focus: function (event, ui) {
            $(event.target).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $(event.target).val(ui.item.label);
            $(event.target).attr("placeId", ui.item.value);
            $(event.target).attr("airport", ui.item.airport);
            clearValidationsFlight();
            return false;
        }
    });

});

function clearValidationsHotel() {
    $("#destino-hotel-error").text("");
    $("#dateFrom-hotel-error").text("");
    $("#dateTo-hotel-error").text("");
}

function clearValidationsFlight() {
    $("#destino-vuelo-origen-error").text("");
    $("#destino-vuelo-destino-error").text("");
    $("#dateFrom-vuelo-error").text("");

}

function validateHotel() {
    clearValidationsHotel();

    var result = true;
    var placeId = $("#strDestinos").attr("placeId");

    var dateFrom = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-first").val()));
    var dateTo = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-last").val()));

    if (isEmpty(placeId)) {
        result = false;
        $("#destino-hotel-error").text("Ingrese un destino valido.");
    }
    if (dateFrom == null || dateFrom == '') {
        result = false;
        $("#dateFrom-hotel-error").text("Debe ingresar una fecha");
    }
    if (dateTo == null || dateTo == '') {
        result = false;
        $("#dateTo-hotel-error").text("Debe ingresar una fecha");
    }

    return result;
}

function validateFlight() {
    clearValidationsFlight();

    var result = true;
    var origenPlaceId = $("#strVuelos-origen").attr("placeId");
    var destinoPlaceId = $("#strVuelos-destino").attr("placeId");
    var dateFrom = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-vuelo-first").val()));


    if (isEmpty(origenPlaceId)) {
        result = false;
        $("#destino-vuelo-origen-error").text("Ingrese un destino valido.");
    }
    if (isEmpty(destinoPlaceId)) {
        result = false;
        $("#destino-vuelo-destino-error").text("Ingrese un destino valido.");
    }
    if (dateFrom == null || dateFrom == '') {
        result = false;
        $("#dateFrom-vuelo-error").text("Debe ingresar una fecha");
    }


    return result;
}

function searchHotels() {

    if (validateHotel()) {
        var placeId = $("#strDestinos").attr("placeId");
        var placeName = $("#strDestinos").val();
        var dateFrom = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-first").val()));
        var dateTo = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-last").val()));
        var url = "http://hoteles.claroviajes.com/search/hotelsearch?placeId={0}&placeName={1}&dateFrom={2} 12:00&dateTo={3} 12:00".replace("{0}", placeId).replace("{1}", placeName).replace("{2}", dateFrom).replace("{3}", dateTo);

        window.location = url;
        //window.open(url,'_blank');
    }
}

function searchFlights() {
    if (validateFlight()) {
        var origenPlaceId = $("#strVuelos-origen").attr("placeId");
        var origenPlaceName = $("#strVuelos-origen").val();
        var origenAirport = $("#strVuelos-origen").attr("airport");

        var destinoPlaceId = $("#strVuelos-destino").attr("placeId");
        var destinoPlaceName = $("#strVuelos-destino").val();
        var destinoAirport = $("#strVuelos-destino").attr("airport");

        var dateFrom = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-vuelo-first").val()));
        var dateTo = $.datepicker.formatDate('yy-mm-dd', $.datepicker.parseDate('dd/mm/yy', $("#campofecha-vuelo-last").val()));

        var url = "";
        if (dateTo == null || dateTo == "")
            url = "http://vuelos.claroviajes.com/search/flightsearch?dId={0}&dAC={1}&dN={2}&aId={3}&aAc={4}&aN={5}&dateFrom={6} 12:00".replace("{0}", origenPlaceId).replace("{1}", origenAirport).replace("{2}", origenPlaceName).replace("{3}", destinoPlaceId).replace("{4}", destinoAirport).replace("{5}", destinoPlaceName).replace("{6}", dateFrom);
        else
            url = "http://vuelos.claroviajes.com/search/flightsearch?dId={0}&dAC={1}&dN={2}&aId={3}&aAc={4}&aN={5}&dateFrom={6} 12:00&dateTo={7} 12:00".replace("{0}", origenPlaceId).replace("{1}", origenAirport).replace("{2}", origenPlaceName).replace("{3}", destinoPlaceId).replace("{4}", destinoAirport).replace("{5}", destinoPlaceName).replace("{6}", dateFrom).replace("{7}", dateTo);
        window.location = url;
        //window.open(url,'_blank');
    }
}

function isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}
