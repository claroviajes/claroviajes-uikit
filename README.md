# claroviajes-uikit

# A react component lib to share between Claroviajes frontends

### To preview in browser

`npm run preview`

This will start a webpack-dev-server in `localhost:8000`

### To build for distribution

`npm run build`

This will compile files into the `lib` folder
